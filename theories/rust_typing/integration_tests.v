From refinedrust Require Import functions int alias_ptr products automation shr_ref mut_ref.

Module test.
Definition bla0 `{typeGS Σ} :=
  (fn(∀ ( *[κ'; κ]) : 2 | ( *[]) : [] | (x) : unit, (λ f, [(κ', κ)]); (λ π, True)) → ∃ y : Z, () @ (uninit PtrSynType) ; λ π, ⌜ (4 > y)%Z⌝).
Definition bla1 `{typeGS Σ} :=
  (fn(∀ ( *[]) : 0 | ( *[]) : [] | x : unit, (λ _, []); (() :@: (uninit PtrSynType)) ; (λ π, True)) → ∃ y : Z, () @ (uninit PtrSynType) ; (λ π, ⌜ (4 > y)%Z⌝)).
Definition bla2 `{typeGS Σ} :=
  (fn(∀ ( *[]) : 0 | ( *[]) : [] | x : unit, (λ _, []); () :@: (uninit PtrSynType), () :@: (uninit PtrSynType) ; (λ π, True)) → ∃ y : Z, () @ (uninit PtrSynType) ; (λ π, ⌜ (4 > y)%Z⌝)).
Definition bla3 `{typeGS Σ} T_st T_rt :=
  (fn(∀ ( *[]) : 0 | ( *[ T_ty]) : [ (T_rt, T_st) ] | (x) : _, (λ _, []); x :@: T_ty, () :@: (uninit PtrSynType) ; (λ π, True)) → ∃ y : Z, () @ (uninit PtrSynType) ; (λ π, ⌜ (4 > y)%Z⌝)).

(* this is a problem, because the type of the function depends on the params, but the _type_ of the function type cannot depend on  it. only the term.

Alternative solutions:
   - we quantify over the xt as well. This seems ugly and like a hack.
     + I guess we could get away with only doing that for function specs.
       ie. we quantify over the xt and then require that it's the same in the extra preconds.
   - we do some weird closed world thing.
   - we refactor the specification format to always universally quantify over the xt of the args.
   - we universally quantify over the rt but require that it's equal to projection of xt
   - we bundle the params type.
     + This doesn't really work because of how we setup the notations.

   Problem: if I can't quantify over it, how will I encode rr frontend specs?
   I guess I could always generically quantify over the refinements r1 .. rn.

   It will be hard to keep the ability to treat args as mixed input/output.
   In general I should move to using projections more.

   But we can keep args clauses as a way to specify the name we can use to refer to an arg, in the first step.
   Then I existentially quantify over the args. (combined params + args)
   ∃ arg1, arg2.
   r1 = ty.(ty_xt) arg1
   r2 = ty.(ty_xt) arg2

   Point: I need to separately existentially quantify over the xts, because it's not in scope for the outside params.

   Problem: I cannot refer to the args in the postcondition.
    -> this is really a problem.


   What is the point of having the params exposed?
   - for traits, do I need to know that the params are consistent between impls?
   - I don't immediately see why we should need it. Try it out, I guess?

*)

(** Testing type parameter instantiation *)
Definition ptr_write `{!LayoutAlg} (T_st : syn_type) : function := {|
  f_args := [("dst", void* ); ("src", use_layout_alg' T_st)];
  f_local_vars := [];
  f_code :=
    <["_bb0" :=
      !{PtrOp} "dst" <-{use_op_alg' T_st} use{use_op_alg' T_st} "src";
      return zst_val
    ]>%E $
    ∅;
  f_init := "_bb0";
|}.

(* Maybe this should also be specced in terms of value? *)
Definition type_of_ptr_write `{!typeGS Σ} (T_rt : Type) (T_st : syn_type) :=
  fn(∀ ( *[]) : 0 | ( *[T_ty]) : [(T_rt, T_st)] | (l, r) : (loc * _), (λ ϝ, []);
      l :@: alias_ptr_t, r :@: T_ty; λ π, (l ◁ₗ[π, Owned false] .@ (◁ uninit (T_ty.(ty_syn_type)))))
    → ∃ () : unit, () @ unit_t; λ π,
        l ◁ₗ[π, Owned false] #$# r @ ◁ T_ty.

Lemma ptr_write_typed `{!typeGS Σ} π T_rt T_st T_ly :
  syn_type_has_layout T_st T_ly →
  ⊢ typed_function π (ptr_write T_st) [] (<tag_type> type_of_ptr_write T_rt T_st).
Proof.
  start_function "ptr_write" ϝ ( [] ) ( [T_ty []] ) ( [l r] ) ( ).
  intros ls_dst ls_src.
  repeat liRStep; liShow.
  Unshelve. all: unshelve_sidecond; sidecond_hook.
  Unshelve. all: unfold_common_defs; try solve_goal.
Qed.

(** If we specialize the type to [int i32], the proof should still work. *)
Definition type_of_ptr_write_int `{!typeGS Σ} :=
  spec_instantiate_typaram [_] 0 eq_refl (int i32) (type_of_ptr_write Z (IntSynType i32)).
Lemma ptr_write_typed_int `{!typeGS Σ} π :
  ⊢ typed_function π (ptr_write (IntSynType i32)) [] (<tag_type> type_of_ptr_write_int).
Proof.
  start_function "ptr_write" ϝ ( [] ) ( [] ) ( [l r] ) ( ).
  intros ls_dst ls_src.
  repeat liRStep; liShow.
  Unshelve. all: unshelve_sidecond; sidecond_hook.
  Unshelve. all: unfold_common_defs; try solve_goal.
Qed.

(** Same for shared references *)
Definition type_of_ptr_write_shrref `{!typeGS Σ} (U_rt : Type) (U_st : syn_type) :=
  (* First add a new type parameter and a new lifetime *)
  fnspec! ( *[κ]) : 1 | ( *[U_ty]) : [(U_rt, U_st)],
    (* Then instantiate the existing type parameter with shr_ref U_ty κ *)
    (type_of_ptr_write (place_rfn U_rt) (PtrSynType) <TY>@{0} shr_ref κ U_ty) <MERGE!>.

Lemma ptr_write_typed_shrref `{!typeGS Σ} π U_rt U_st :
  ⊢ typed_function π (ptr_write (PtrSynType)) [] (<tag_type> type_of_ptr_write_shrref U_rt U_st).
Proof.
  start_function "ptr_write" ϝ ( [ulft_a []]  ) ( [U_ty []] ) ( [l r] ) ( ).
  intros ls_dst ls_src.
  repeat liRStep; liShow.
  Unshelve. all: unshelve_sidecond; sidecond_hook.
  Unshelve. all: unfold_common_defs; try solve_goal.
Qed.
End test.
